# Acceso a Base de Datos. PDO (PHP Data Objects)

## Preparando el entorno

### 1. Importación de la base de datos
 El primer paso para la preparación del entorno es la importación del ``schema`` de la base de dades contenida en el [siguiente](https://github.com/rhidalgocipfpbatoi/iaw-ud3-classwork-master/blob/master/database/crm_db.sql) script.  
La opción más fácil es volcar el contenido del fichero mediante el cliente de línea de comandos ``mysql client``, y utilizando el usuario por defecto del sistema.

```bash
    sudo mysql -u'root' < database/crm_db.sql 
```

~~~
    La instalación por defecto del SGBD mysql en distribuciones "Ubuntu Server ~18.04" proporciona un usuario "root" sin "contraseña" con grants "ALL_PRIVILEGES"
    y que puede establecer solo conexiones locales (desde el mismo host) a través del socket LINUX situado en "/var/run/mysqld/mysqld.sock"
~~~

### 2. Creación del usuario de conexión
Una vez importada la base de datos crearemos un usuario para nuestra aplicación ``php`` debemos tener en cuenta que:
- Debe ser un usuario con los mínimos privilegios que requiera nuestra aplicación para que, en caso que existiera alguna vulnerabilidad,
 el daño fuera mínimo.
- No debemos compartir usuarios con distintas aplicaciones, en otro caso, estariamos introduciendo acoplamientos y vulnerabilidades
entre aplicaciones.
- El usuario que creemos solo tendrá permisos de acceso desde el host o la ip en la que se vaya a ejecutar nuestra aplicación. En nuestro
caso ``localhost``.

```bash
  
    CREATE USER descriptive-user-name@'ip-from-connection-host' IDENTIFIED BY 'strong-password';
    
    #Creamos el usuario
    CREATE USER 'alecogi-web'@'%' IDENTIFIED BY '123456789'
    
    #Le concedemos todos los privileios para la base de datos creada
    GRANT ALL PRIVILEGES ON crm_db.* TO 'alecogi-web'@'%'
    
```

El [siguiente enlace](https://www.digitalocean.com/community/tutorials/como-instalar-mysql-en-ubuntu-18-04-es) nos propociona un resumen sobre una instalación básica del servidor **mysql** en **Ubuntu Server 18.04**

~~~
Debemos tener en cuenta que en algunas distribuciones mysql se encuentra configurado por defecto para atender solamente peticiones
del mismos host **localhost** si queremos conectarnos desde otro host deberemos bindear el servicio a la interfaz de red correspondiente.

$sudo vim /etc/mysql/mysql.conf.d/mysqld.cnf
bind-address            = 0.0.0.0
~~~

### 3. Instalación del driver nativo php-mysql

Como último paso llevaremos acabo la instalación del driver de mysql para php
```bash
$sudo apt install php-mysql
```
**Nota** no olvides reinicar el servidor php

## Actividad 1

**A)** Crea un directorio ``resources`` y crea script ``database-params.php`` en el que guardaremos los parámetros de acceso a la base de datos.

```php
<?php
   $dataBaseParams = [
          "host" => ... ,
          "user" => ... ,
          "password" => ... ,
          "database" => ...
   ]; 
```
**B)** Crea un script ``connection.php`` en el que:
  - A partir de la inclusión del fichero anterior cree una conexión con la BD.
  - Crea una segundo script ``connection_error.php``. En este caso debe utilizar unas credenciales erróneas.
    Muestra por pantalla el mensaje y código ``SQLSTATE`` correspondiente.
 
```php
<?php   
    
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
    
    require __DIR__ . "/../resources/database-params.php";
    
     try {
    
         // Accedemos a cada uno de los parámetros de acceso a la base de datos
         $usuario = $dataBaseParams['user'];
         $contraseña =  $dataBaseParams['password'];
         $ipServidor = $dataBaseParams['host'];
         $databaseName = $dataBaseParams['database'];
         
         //Creamos la conexión con la base de datos a partir de los parámetros leidos
         $conexión = new PDO("mysql:host=$ipServidor;dbname=$databaseName", $usuario, $contraseña);
         echo "Conexion establecida ";
    
        } catch (PDOException $e) {
   
            echo "Error en la conexión con la Base de datos " . $e->getMessage() . "<br/>";
            die();
    
        }
```
~~~
Todos los scripts que creemos incluiran el fichero ``database-params.php`` para obtener los párametros de conexión
con la base de datos
~~~

**C)** Crea un script ``listado_usuarios.php`` que lea y muestre todos los usuarios de la base de datos. Debe mostrarlos
 en una elemento ``<html>``

```php
<?php  

    //incluimos el fichero creado en el apartado anterior de forma que ya disponemos de una conexión a la BD
    include __DIR__. "/connection.php";

    //Creamos el select de acceso a la base de datos
    $sql = "SELECT id,firstName, lastName, phoneNumber, active, createdOn FROM User";

    //Ejecutamos la sentencia
    $listadoUsuarios = $conexión->query($sql);
    
    //Iteramos a través del array de resultados
    foreach ($listadoUsuarios as $row) {
        
        //Accedemos a cada uno de los elementos
        echo($row['firstName']);
        echo($row['lastName']);
        
    }
    
```

**D)** Crea un formulario ``form.html``  para obtener los datos a insertar respecto a un nuevo usuario y crea el script ``insertar_usuario.php`` para almacenar en la base de datos la información del nuevo usuario introducido a través del formulario. El formulario debe ser como el que se muestra en la imagen siguiente.

Imagen ejemplo del formulario de inserción de datos de usuario

![](image/ImageForm.png)


## Ampliación


**E)** Crea un formulario ``insert_image.html``  para insertar la ruta de la imagen seleccionada en el formulario en el registro de un usuario ya existente en la base de datos, para ello crea el script ``insert_photo.php``. Deberas crear un nuevo directorio en el proyecto para almacenar las imagenes y añadir un campo más a la base de datos para almacenar el nombre de la imagen. Por último, una vez realizada la inserción de la imagen muestra el nombre, apellidos e imagen de todos los usuarios, los usuarios que no tengan una foto deberan utilizar una por defecto.

**Nota** 

Busca información respecto a:  move_uploaded_file($_FILES...)

Revisa los permisos de acceso al nuevo directorio creado.

Imagen ejemplo del formulario de inserción de la imagen

![](image/insertHtml.png)

Imagen ejemplo listado usuario con imagen


![](image/insertphp.png)







